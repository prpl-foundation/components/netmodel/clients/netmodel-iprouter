# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.3.1 - 2024-03-21(14:37:25 +0000)

### Fixes

- IPv6 connectivity is KO when RA is missing Prefix Information

## Release v0.3.0 - 2023-10-03(06:57:51 +0000)

### New

- [amx][conmon] ConMon must support arp(ipv4) and icmpv6(ipv6)

## Release v0.2.0 - 2023-06-01(10:52:59 +0000)

### New

- [CDROUTER][IPv6] The Box is still advertising itself as router at reception on WAN of RA with router lifetime 0

## Release v0.1.4 - 2023-05-16(15:32:23 +0000)

### Fixes

- Sync correct Managed and Other parameter

## Release v0.1.3 - 2023-05-04(14:07:31 +0000)

### Fixes

- Remove vendor prefix

## Release v0.1.2 - 2023-04-04(10:44:33 +0000)

### Fixes

- Create an event when a renew is received

## Release v0.1.1 - 2022-03-18(07:31:25 +0000)

### Other

- Opensource component

## Release v0.1.0 - 2022-03-17(10:22:58 +0000)

### New

- implement iprouter client and iprouter mib

