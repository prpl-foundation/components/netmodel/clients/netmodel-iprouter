/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_path.h>
#include <amxs/amxs.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <netmodel/client.h>
#include <netmodel/mib.h>
#include "mib_iprouter.h"

#define ME "iprouter_mib"
#define MOD "iprouter"
#define string_empty(x) ((x == NULL) || (*x == '\0'))

static void update_flag(const char* netmodel_intf, const char* flag, bool set) {
    amxc_var_t args;
    amxc_var_t ret;
    const char* function = set ? "setFlag" : "clearFlag";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);

    if(AMXB_STATUS_OK != amxb_call(netmodel_get_amxb_bus(), netmodel_intf, function, &args, &ret, 5)) {
        SAH_TRACEZ_ERROR(ME, "Failed to call %s%s(flag='%s')", netmodel_intf, function, flag);
        goto exit;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static amxs_status_t router_lifetime_action_cb(const amxs_sync_entry_t* sync_entry, amxs_sync_direction_t direction, amxc_var_t* data, void* priv) {
    amxd_path_t path;
    amxs_status_t status = amxs_status_unknown_error;
    const char* netmodel_intf = GET_CHAR(data, "path");
    uint32_t lifetime = GETP_UINT32(data, "parameters.RouterLifetime");
    char* last = NULL;
    bool is_up = false;

    when_false((direction == amxs_sync_a_to_b), exit);

    is_up = lifetime != 0;

    amxd_path_init(&path, netmodel_intf);
    last = amxd_path_get_last(&path, true); // remove 'IPv6Router.' to set flags
    free(last);
    last = NULL;

    update_flag(amxd_path_get(&path, AMXD_OBJECT_TERMINATE), "iprouter-up", is_up);

    amxd_path_clean(&path);

    status = amxs_sync_param_copy_action_cb(sync_entry, direction, data, priv);
exit:
    return status;
}

static amxs_status_t add_parameters(amxs_sync_ctx_t* ctx) {
    amxs_status_t status = amxs_status_unknown_error;
    amxs_sync_object_t* obj = NULL;

    status = amxs_sync_ctx_add_new_copy_param(ctx, "ManagedAddressConfiguration", "Managed", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "OtherConfiguration", "Other", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_param(ctx, "RouterLifetime", "RouterLifetime", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, router_lifetime_action_cb, ctx);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "SourceRouter", "SourceRouter", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_new_copy(&obj, "Option.", "Option.", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "Tag", "Tag", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "Value", "Value", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_object_add_new_copy_param(obj, "LastUpdate", "LastUpdate", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_object(ctx, obj);

    return status;
}

static char* find_instance(amxb_bus_ctx_t* bus, const char* interface_path) {
    char* instance = NULL;
    amxc_var_t ret;
    amxc_string_t search_path;
    const char* key = NULL;
    int rv = -1;

    amxc_var_init(&ret);
    amxc_string_init(&search_path, 0);

    if(string_empty(interface_path)) {
        SAH_TRACEZ_ERROR(ME, "Missing interface path");
        goto exit;
    }

    amxc_string_setf(&search_path, "Routing.RouteInformation.InterfaceSetting.[Interface == '%s'].", interface_path);
    rv = amxb_get(bus, amxc_string_get(&search_path, 0), 0, &ret, 5);
    if(AMXB_STATUS_OK != rv) {
        SAH_TRACEZ_ERROR(ME, "Failed to get instance %s, reason: %d", amxc_string_get(&search_path, 0), rv);
        goto exit;
    }

    key = amxc_var_key(GETP_ARG(&ret, "0.0"));
    if(string_empty(key)) {
        SAH_TRACEZ_ERROR(ME, "Instance %s not found", amxc_string_get(&search_path, 0));
        goto exit;
    }

    instance = strdup(key);

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&search_path);
    return instance;
}

static char* find_object_b(const char* netmodel_intf) {
    amxc_string_t str;
    char* obj_b = NULL;

    amxc_string_init(&str, 0);

    amxc_string_setf(&str, "%sIPv6Router.", netmodel_intf);
    obj_b = amxc_string_take_buffer(&str);

    amxc_string_clean(&str);
    return obj_b;
}

static netmodel_mib_t mib = {
    .name = ME,
    .root = "Routing.",
    .flags = NETMODEL_MIB_DONT_LINK_INTERFACES |
        NETMODEL_MIB_ONLY_MY_PARAMETERS,
    .add_sync_parameters = add_parameters,
    .find_instance_to_sync = find_instance,
    .find_object_b_to_sync = find_object_b,
};

int mib_added(UNUSED const char* const function_name,
              amxc_var_t* args,
              UNUSED amxc_var_t* ret) {
    netmodel_subscribe(args, &mib);

    return 0;
}

int mib_removed(UNUSED const char* const function_name,
                amxc_var_t* args,
                UNUSED amxc_var_t* ret) {
    netmodel_unsubscribe(args, &mib);
    update_flag(GET_CHAR(args, "object"), "iprouter-up", false);

    return 0;
}

static AMXM_CONSTRUCTOR mib_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "Mib init triggered");

    amxm_module_register(&mod, so, MOD);
    amxm_module_add_function(mod, "mib-added", mib_added);
    amxm_module_add_function(mod, "mib-removed", mib_removed);

    netmodel_initialize();

    return 0;
}

static AMXM_DESTRUCTOR mib_clean(void) {
    SAH_TRACEZ_INFO(ME, "Mib clean triggered");

    netmodel_cleanup();

    return 0;
}
